package ru.t1.kruglikov.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.Connection;

public interface IConnectionService {

    @NotNull
    EntityManagerFactory getEntityManagerFactory();

    @NotNull
    EntityManager getEntityManager();

}

