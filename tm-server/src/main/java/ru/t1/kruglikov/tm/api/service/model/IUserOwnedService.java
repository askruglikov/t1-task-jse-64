package ru.t1.kruglikov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.kruglikov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    @Nullable
    M add(
            @Nullable String userId,
            @Nullable M model
    );

    @NotNull
    List<M> findAll(@Nullable String userId);

    @Nullable
    M findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    M findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    M removeOne(
            @Nullable String userId,
            @Nullable M model
    );

    @Nullable
    M removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    M removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    void removeAll(@Nullable String userId);

    long getSize(@Nullable String userId);

    boolean existsById(
            @Nullable String userId,
            @Nullable String id
    );

}
