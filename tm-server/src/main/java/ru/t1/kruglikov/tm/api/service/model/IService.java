package ru.t1.kruglikov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.model.AbstractModelDTO;
import ru.t1.kruglikov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull
    M add(@Nullable M model);

    @NotNull
    Collection<M> add(@Nullable Collection<M> models);

    @NotNull
    Collection<M> set(@Nullable Collection<M> models);

    @NotNull
    List<M> findAll();

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    @NotNull
    M removeOne(@Nullable M model);

    @Nullable
    M removeOneById(@Nullable String id);

    @Nullable
    M removeOneByIndex(@Nullable Integer index);

    void removeAll();

    long getSize();

    boolean existsById(String id);

}