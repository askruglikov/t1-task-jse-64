package ru.t1.kruglikov.tm.constant.model;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.model.UserDTO;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.model.User;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@UtilityClass
public final class UserTestData {

    @NotNull
    public final static User ADMIN = new User("admin_test", "admin", Role.ADMIN);

    @NotNull
    public final static User USER1 = new User("USER_01_test", "user01", "user01@address.ru");

    @NotNull
    public final static User USER2 = new User("USER_02_test", "user02", "user02@address.ru");

    @NotNull
    public final static List<User> USER_LIST = Arrays.asList(ADMIN, USER1, USER2);

    @NotNull
    public final static List<User> USER_LIST2 = Arrays.asList(USER1);

    @NotNull
    public final static List<User> ADMIN_LIST = Collections.singletonList(ADMIN);

    @Nullable
    public final static User NULL_USER = null;

    @Nullable
    public final static String NULL_USER_ID = null;

    @Nullable
    public final static List<User> NULL_USER_LIST = null;

}
