package ru.t1.kruglikov.tm.api.provider;

import org.jetbrains.annotations.NotNull;

public interface IConnectionProvider {

    @NotNull
    String getHost();

    @NotNull
    String getPort();

}
