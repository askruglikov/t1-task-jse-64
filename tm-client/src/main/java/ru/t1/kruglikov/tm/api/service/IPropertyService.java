package ru.t1.kruglikov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.kruglikov.tm.api.provider.IConnectionProvider;

public interface IPropertyService extends ISaltProvider, IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

}
