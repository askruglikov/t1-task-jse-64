package ru.t1.kruglikov.tm.listener;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.api.IListener;
import ru.t1.kruglikov.tm.api.service.ILocatorService;
import ru.t1.kruglikov.tm.enumerated.Role;

@Getter
@Setter
@Component
public abstract class AbstractListener implements IListener {

    @Nullable
    @Autowired
    protected ILocatorService locatorService;

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    protected String getToken() {
        return getLocatorService().getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        getLocatorService().getTokenService().setToken(token);
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();

        String displayName = "";
        if (name != null && !name.isEmpty()) displayName += name;
        if (argument != null && !argument.isEmpty()) displayName += ", " + argument;
        if (description != null && !description.isEmpty()) displayName += ": " + description;

        return displayName;
    }

}
