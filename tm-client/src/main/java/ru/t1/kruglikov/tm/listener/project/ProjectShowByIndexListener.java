package ru.t1.kruglikov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.model.ProjectDTO;
import ru.t1.kruglikov.tm.dto.request.project.ProjectShowByIndexRequest;
import ru.t1.kruglikov.tm.event.ConsoleEvent;
import ru.t1.kruglikov.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIndexListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-show-by-index";

    @NotNull
    public static final String DESCRIPTION = "Display project by index.";

    @Override
    @EventListener(condition = "@projectShowByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[SHOW PROJECT BY INDEX]");

        System.out.println("Enter project index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @Nullable final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(getToken());
        request.setIndex(index);
        @Nullable final ProjectDTO project = projectEndpoint.showByIndex(request).getProject();

        showProject(project);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
